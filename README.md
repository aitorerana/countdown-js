# countdown #

This is jQuery library to display a countdown clock.

### Requirements ###

This library requires to import **[jQuery](https://jquery.com/ "jQuery")** before calling the function.

### Configuration ###

1. Put your code un your HTML: ```<div class="countdown"></div>```
2. And then import the script: ```<script src="countdown.js"></script>```

### Parameters ###

* **data-year**: The year to define the goal date. Default current year.
* **data-month**: The month to define the goal date. Default current month.
* **data-day**: The day to define the goal date. Default current day.
* **data-hour**: The hour to define the goal date. Default 0.
* **data-minute**: The minute to define the goal date. Default 0.
* **data-second**: The second to define the goal date. Default 0.
* **data-year-units-singular**: Text to display after years in singular. Default 'year'.
* **data-year-units-plural**: Text to display after years in plural. Default 'years'.
* **data-month-units-singular**: Text to display after months in singular. Default 'month'.
* **data-month-units-plural**: Text to display after months in plural. Default 'months'.
* **data-day-units-singular**: Text to display after days in singular. Default 'day'.
* **data-day-units-plural**: Text to display after days in plural. Default 'days'.
* **data-hour-units-singular**: Text to display after hours in singular. Default 'hour'.
* **data-hour-units-plural**: Text to display after hours in plural. Default 'hours'.
* **data-minute-units-singular**: Text to display after minutes in singular. Default 'minute'.
* **data-minute-units-plural**: Text to display after minutes in plural. Default 'minutes'.
* **data-second-units-singular**: Text to display after seconds in singular. Default 'second'.
* **data-second-units-plural**: Text to display after seconds in plural. Default 'seconds'.
* **data-year-class**: Class to the year object.
* **data-month-class**: Class to the month object.
* **data-day-class**: Class to the day object.
* **data-hour-class**: Class to the hour object.
* **data-minute-class**: Class to the minute object.
* **data-second-class**: Class to the second object.
* **data-year-month-day-class**: Class to the year-month-day object. This is defined to show thouse object in the first line.
* **data-hour-minute-second-class**: Class to the hour-minute-second object. This is defined to show thouse object in the second line.

### Run the project ###

Here a simple command to start this project localy: **php -S localhost:7000**

### Author ###

[Aitor Eraña](http://cv.aitorerana.com/ "CV aitorerana")