$(document).ready(function() {

    setInterval(updateCounters, 500);

    function updateCounters() {
        $('.countdown').each(function () { updateCounter(this); });
    }

    function updateCounter(element) {
        var { year: currentYear, month: currentMonth, day: currentDay } = deconstructDate(new Date());

        var year = $(element).data('year') ? $(element).data('year') : currentYear;
        var month = $(element).data('month') ? $(element).data('month') : currentMonth;
        var day = $(element).data('day') ? $(element).data('day') : currentDay;
        var hour = $(element).data('hour') ? $(element).data('hour') : 0;
        var minute = $(element).data('minute') ? $(element).data('minute') : 0;
        var second = $(element).data('second') ? $(element).data('second') : 0;

        var startDate = new Date();
        var endDate = new Date(+year, +month-1, +day, +hour, +minute, +second);

        var diffObject = dateDiff(startDate, endDate);
        var diffString = dateDiff2String(diffObject, $(element));

        $(element).html(diffString);
    }

    function dateDiff(dateA, dateB) {

        if (dateA > dateB) {
            return {
                'years': 0,
                'months': 0,
                'days': 0,
                'hours': 0,
                'minutes': 0,
                'seconds': 0,
            };
        }

        var { year: yearA, month: monthA, day: dayA, hour: hourA, minute: minuteA, second: secondA } = deconstructDate(dateA);
        var { year: yearB, month: monthB, day: dayB, hour: hourB, minute: minuteB, second: secondB } = deconstructDate(dateB);

        var monthMaxDaysA = getDaysInMonth(yearA, monthA);

        var isAbeforeBForSecond = secondA == secondB ? true : secondA <= secondB;
        var isAbeforeBForMinute = minuteA == minuteB ? isAbeforeBForSecond : minuteA <= minuteB;
        var isAbeforeBForHour = hourA == hourB ? isAbeforeBForMinute : hourA <= hourB;
        var isAbeforeBForDay = dayA == dayB ? isAbeforeBForHour : dayA <= dayB;
        var isAbeforeBForMonth = monthA == monthB ? isAbeforeBForDay : monthA <= monthB;
        var isAbeforeBForYear = yearA == yearB ? isAbeforeBForMonth : yearA <= yearB;

        var diffYears = yearB - yearA - (isAbeforeBForYear && isAbeforeBForMonth ? 0 : 1);
        var diffMonths = (isAbeforeBForMonth ? (monthB - monthA) : (12 - monthA + monthB)) - (isAbeforeBForMonth && isAbeforeBForDay ? 0 : 1);
        var diffDays = (isAbeforeBForDay ? (dayB - dayA) : (monthMaxDaysA - dayA + dayB)) - (isAbeforeBForDay && isAbeforeBForHour ? 0 : 1);
        var diffHours = (isAbeforeBForHour ? (hourB - hourA) : (24 - hourA + hourB)) - (isAbeforeBForHour && isAbeforeBForMinute ? 0 : 1);
        var diffMinutes = (isAbeforeBForMinute ? (minuteB - minuteA): (60 - minuteA + minuteB)) - (isAbeforeBForMinute && isAbeforeBForSecond ? 0 : 1);
        var diffSeconds = isAbeforeBForSecoånd ? (secondB - secondA) : (60 - secondA + secondB);

        return {
            'years': typeof diffYears !== 'undefined' && diffYears > 0 ? diffYears : 0,
            'months': typeof diffMonths !== 'undefined' && diffMonths > 0 ? diffMonths : 0,
            'days': typeof diffDays !== 'undefined' && diffDays > 0 ? diffDays : 0,
            'hours': typeof diffHours !== 'undefined' && diffHours > 0 ? diffHours : 0,
            'minutes': typeof diffMinutes !== 'undefined' && diffMinutes > 0 ? diffMinutes : 0,
            'seconds': typeof diffSeconds !== 'undefined' && diffSeconds > 0 ? diffSeconds : 0,
            'helpNotes': typeof helpNotes !== 'undefined' ? helpNotes : '',
        }
    }

    function dateDiff2String(diffObject, context) {
        var { years, months, days, hours, minutes, seconds, helpNotes } = diffObject;

        var yearUnitsSingular = context.data('year-units-singular') ? context.data('year-units-singular') : 'year';
        var yearUnitsPlural = context.data('year-units-plural') ? context.data('year-units-plural') : 'years';
        var monthUnitsSingular = context.data('month-units-singular') ? context.data('month-units-singular') : 'month';
        var monthUnitsPlural = context.data('month-units-plural') ? context.data('month-units-plural') : 'months';
        var dayUnitsSingular = context.data('day-units-singular') ? context.data('day-units-singular') : 'day';
        var dayUnitsPlural = context.data('day-units-plural') ? context.data('day-units-plural') : 'days';
        var hourUnitsSingular = context.data('hour-units-singular') ? context.data('hour-units-singular') : 'hour';
        var hourUnitsPlural = context.data('hour-units-plural') ? context.data('hour-units-plural') : 'hours';
        var minuteUnitsSingular = context.data('minute-units-singular') ? context.data('minute-units-singular') : 'minute';
        var minuteUnitsPlural = context.data('minute-units-plural') ? context.data('minute-units-plural') : 'minutes';
        var secondUnitsSingular = context.data('second-units-singular') ? context.data('second-units-singular') : 'second';
        var secondUnitsPlural = context.data('second-units-plural') ? context.data('second-units-plural') : 'seconds';

        var yearUnits = years <= 1 ? yearUnitsSingular : yearUnitsPlural;
        var monthUnits = months <= 1 ? monthUnitsSingular : monthUnitsPlural;
        var dayUnits = days <= 1 ? dayUnitsSingular : dayUnitsPlural;
        var hourUnits = hours <= 1 ? hourUnitsSingular : hourUnitsPlural;
        var minuteUnits = minutes <= 1 ? minuteUnitsSingular : minuteUnitsPlural;
        var secondUnits = seconds <= 1 ? secondUnitsSingular : secondUnitsPlural;

        var yearClass = context.data('year-class') ? context.data('year-class') : '';
        var monthClass = context.data('month-class') ? context.data('month-class') : '';
        var dayClass = context.data('day-class') ? context.data('day-class') : '';
        var hourClass = context.data('hour-class') ? context.data('hour-class') : '';
        var minuteClass = context.data('minute-class') ? context.data('minute-class') : '';
        var secondClass = context.data('second-class') ? context.data('second-class') : '';

        var yearsText = years > 0 ? '<span class="' + yearClass + '"><span class="countdown-years">' + years + '</span> <span class="countdown-year-units" translate="' + yearUnits + '">' + yearUnits + '</span></span>' : '';
        var monthsText = years > 0 || months > 0 ? '<span class="' + monthClass + '"><span class="countdown-months">' + months + '</span> <span class="countdown-month-units" translate="' + monthUnits + '">' + monthUnits + '</span></span>' : '';
        var daysText = years > 0 || months > 0 || days > 0 ? '<span class="' + dayClass + '"><span class="countdown-days">' + days + '</span> <span class="countdown-day-units" translate="' + dayUnits + '">' + dayUnits + '</span></span>' : '';
        var hoursText = '<span class="' + hourClass + '"><span class="countdown-hours">' + hours + '</span> <span class="countdown-hour-units" translate="' + hourUnits + '">' + hourUnits + '</span></span>';
        var minutesText = '<span class="' + minuteClass + '"><span class="countdown-minutes">' + minutes + '</span> <span class="countdown-minute-units" translate="' + minuteUnits + '">' + minuteUnits + '</span></span>';
        var secondsText = '<span class="' + secondClass + '"><span class="countdown-seconds">' + seconds + '</span> <span class="countdown-second-units" translate="' + secondUnits + '">' + secondUnits + '</span></span>';

        var yearMonthDayClass = context.data('year-month-day-class') ? context.data('year-month-day-class') : '';
        var hourMinuteSecondClass = context.data('hour-minute-second-class') ? context.data('hour-minute-second-class') : '';

        return (helpNotes ?? '') + '<span class="' + yearMonthDayClass + '">' + yearsText + monthsText + daysText + '</span><span class="' + hourMinuteSecondClass + '">' + hoursText + minutesText + secondsText + '</span>';
    }

    function getDaysInMonth(year, month) {
        return new Date(year, month, 0).getDate();
    }

    function deconstructDate(date) {
        return {
            'year': date.getFullYear(),
            'month': date.getMonth() + 1,
            'day': date.getDate(),
            'hour': date.getHours(),
            'minute': date.getMinutes(),
            'second': date.getSeconds(),
        }
    }

});