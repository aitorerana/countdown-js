<!DOCTYPE html>
<html>
        <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width">
        <title>countdown</title>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    </head>
    <body>
        <div
            class="countdown"
            data-year="2021"
            data-year-units="y"
            data-month="08"
            data-day="18"
            data-second-units="sec"
        ></div>
        <script src="countdown.js"></script>
    </body>
</html>